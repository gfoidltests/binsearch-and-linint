#include "types.h"

double linint_core(double x0, double y0, double x1, double y1, double x);

point2 linint(point2 p0, point2 p1, double x)
{
	point2 p;

	p.x = x;
	p.y = linint_core(p0.x, p0.y, p1.x, p1.y, x);

	return p;
}

point3 bilinint(point3 p00, point3 p01, point3 p10, point3 p11, point2 x)
{
	point3 p;
	double z0, z1;

	z0 = linint_core(p00.x, p00.z, p01.x, p01.z, x.x);
	z1 = linint_core(p10.x, p10.z, p11.x, p11.z, x.x);

	p.x = x.x;
	p.y = x.y;
	p.z = linint_core(p00.y, z0, p10.y, z1, x.y);

	return p;
}

double linint_core(double x0, double y0, double x1, double y1, double x)
{
	return y0 + (y1 - y0) / (x1 - x0) * (x - x0);
}