#include <stdio.h>
#include "binsearch.h"
//#include "types.h"				// ist nicht n�tig, da dies bei "linint.h" importiert wird
#include "linint.h"

int main(void)
{
	int arr1[] = {1,2,3,5,6,7,8,9,10};
	double arr2[] = {1,2,3,4,5,6,7,8,9,10};
	point2 
		p0 = {0, 0}, 
		p1 = {5, 10};
	point3 
		p00 = {1.0, 4.0, 1.0},
		p01 = {3.0, 4.0, 2.0},
		p10 = {1.0, 6.0, 3.0},
		p11 = {3.0, 6.0, 4.0};

	printf("index von %2d = %2d\n", 4, binsearch_i(4, arr1, 9));
	printf("index von %2d = %2d\n", 10, binsearch_i(10, arr1, 10));
	printf("\n");
	printf("index von %2.1f = %2d\n", 3.1, binsearch_d(3, arr2, 10));
	printf("index von %2.1f = %2d\n", 10.0, binsearch_d(10, arr2, 10));
	printf("\n");

	printf("linint (0,0) - (5,10) bei x = 2.5 -> %2.1f\n", linint(p0, p1, 2.5).y);
	printf("\n");

	p0.x = 2;
	p0.y = 5;
	printf("bilinint -> %2.1f (expected: 2.5)\n", bilinint(p00, p01, p10, p11, p0).z);
}