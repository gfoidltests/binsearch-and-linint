int binsearch_i(int x, int arr[], int n)
{
	int low, high, mid;

	low = 0;
	high = n -1;

	while (low <= high)
	{
		//mid = (low + high) / 2;
		mid = low + ((high - low) >> 1);	// so kann bei großen Feldern kein Überlauf passieren

		if (x < arr[mid])
			high = mid - 1;
		else if (x > arr[mid])
			low = mid + 1;
		else
			return mid;
	}

	return -mid;
}

int binsearch_d(double x, double arr[], int n)
{
	int low, high, mid;

	low = 0;
	high = n -1;

	while (low <= high)
	{
		//mid = (low + high) / 2;
		mid = low + ((high - low) >> 1);	// so kann bei großen Feldern kein Überlauf passieren

		if (x < arr[mid])
			high = mid - 1;
		else if (x > arr[mid])
			low = mid + 1;
		else
			return mid;
	}

	return -mid;
}