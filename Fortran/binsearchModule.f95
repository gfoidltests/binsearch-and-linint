module binsearchModule
	implicit none
	
	interface binsearch
		module procedure binsearch_i
		module procedure binsearch_d
	end interface
	
	contains
		integer function binsearch_i(x, arr)
			integer, intent(in)					:: x
			integer, dimension(:), intent(in)	:: arr
			integer 							:: low, high, mid
			
			low = 1
			high = size(arr)
			
			do while (low <= high)
				! mid = (low + high) / 2
				mid = low + (high - low) / 2	! so kann bei großen Feldern kein Überlauf passieren
				
				if (x < arr(mid)) then
					high = mid -1
				else if (x > arr(mid)) then
					low = mid + 1
				else 
					binsearch_i = mid;
					return
				end if
			end do
			
			binsearch_i = -mid
		end function binsearch_i
		
		integer function binsearch_d(x, arr)
			double precision, intent(in)				:: x
			double precision, dimension(:), intent(in)	:: arr
			integer 									:: low, high, mid
			
			low = 1
			high = size(arr)
			
			do while (low <= high)
				! mid = (low + high) / 2
				mid = low + (high - low) / 2	! so kann bei großen Feldern kein Überlauf passieren
				
				if (x < arr(mid)) then
					high = mid -1
				else if (x > arr(mid)) then
					low = mid + 1
				else 
					binsearch_d = mid;
					return
				end if
			end do
			
			binsearch_d = -mid
		end function binsearch_d
end module binsearchModule