program main
	use binsearchModule
	use linintModule
	
	implicit none
	integer, parameter				:: n = 10
	integer, dimension(n-1)			:: arr1 = (/ 1, 2, 3, 5, 6, 7, 8, 9, 10 /)
	double precision, dimension(n)	:: arr2 = (/ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 /)
	type(point2)					:: p1 = point2(0, 0), 		&
									   p2 = point2(5, 10), 		&
									   p
	type(point3)					:: p11 = point3(1, 4, 1),	&
									   p12 = point3(3, 4, 2),	&
									   p21 = point3(1, 6, 3),	&
									   p22 = point3(3, 6, 4),	&
									   p3
	
	write(*,901) 'index von ', 4, ' = ', binsearch(4, arr1)
	write(*,901) 'index von ', 10, ' = ', binsearch(10, arr1)
	write(*,*)
	write(*,902) 'index von ', 3.1d0, ' = ', binsearch(3d0, arr2)
	write(*,902) 'index von ', 10._8, ' = ', binsearch(10._8, arr2)
	write(*,*)
	
	p = linint(p1, p2, 2.5d0)
	write(*,'(a,f4.1)') 'linint (0,0) - (5,10) bei x = 2.5 -> ', p%y
	
	p3 = bilinint(p11, p12, p21, p22, point2(2, 5))
	write(*,'(a,f4.1,a)') 'bilinint -> ', p3%z, ' (expected: 2.5)'
	
901 format(a,i2,a,i2)
902 format(a,f4.1,a,i2)
end program