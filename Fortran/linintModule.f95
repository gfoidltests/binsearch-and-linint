module linintModule
	implicit none
	
	type point2
		double precision	:: x, y
	end type
	
	type point3
		double precision	:: x, y, z
	end type
	
	contains
		function linint(p1, p2, x)
			type(point2)					:: linint
			type(point2), intent(in)		:: p1, p2
			double precision				:: x
			
			linint%x = x
			linint%y = linint_core(p1%x, p1%y, p2%x, p2%y, x)
		end function linint
		
		function bilinint(p11, p12, p21, p22, x)
			type(point3)					:: bilinint
			type(point3), intent(in)		:: p11, p12, p21, p22
			type(point2)					:: x
			double precision				:: z1, z2
			
			z1 = linint_core(p11%x, p11%z, p12%x, p12%z, x%x)
			z2 = linint_core(p21%x, p21%z, p22%x, p22%z, x%x)
			
			bilinint%x = x%x
			bilinint%y = x%y
			bilinint%z = linint_core(p11%y, z1, p21%y, z2, x%y)
		end function bilinint
		
		double precision function linint_core(x1, y1, x2, y2, x)
			double precision, intent(in)	:: x1, y1, x2, y2, x
			
			linint_core = y1 + (y2 - y1) / (x2 - x1) * (x - x1)
		end function linint_core
end module linintModule